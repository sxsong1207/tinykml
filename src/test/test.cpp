#include <iostream>

#include <pugixml/pugixml.hpp>
#include <pugixml/pugiconfig.hpp>

#include <miniz/miniz.h>
#include <miniz/miniz_zip.h>
#include <fstream>

#include <tinykml/tinykml.h>
using namespace std;

int main(int argc, char **argv) {

  string KMZFILE = "./testdata/CatalogProducts.kmz";
  std::vector<string> kmlfilename, kmlcontent;
  size_t kml_succ = readKMX(KMZFILE, kmlfilename, kmlcontent);

  pugi::xml_document doc;
  pugi::xml_parse_result result = doc.load_string(kmlcontent.front().c_str());
  if (!result)
    return -1;

  PlacemarkWalker pnw;
  doc.traverse(pnw);

  for (int i = 0; i < pnw.placemarks().size(); ++i) {
    PlacemarkWalker::PlacemarkStruct &ps = pnw.placemarks()[i];
    cout << ps.path << endl;
    cout << ps.name << endl;
    cout << ps.id << endl;

    for (int j = 0; j < ps.polygon.size(); ++j) {
      std::vector<double> &p = ps.polygon[j];
      cout << "  " << p[0] << ", " << p[1] << endl;
    }
  }
  return 0;
}