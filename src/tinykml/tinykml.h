// Copyright 2018 Shaun Song <sxsong1207@qq.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#ifndef TINYKML_TINYKML_H
#define TINYKML_TINYKML_H

#include <iostream>

#include <pugixml/pugixml.hpp>
#include "utils.h"

size_t readKMZ(std::string path, std::vector<std::string> &kmlfilename, std::vector<std::string> &kmlcontent) {
  mz_zip_archive zip_archive;
  memset(&zip_archive, 0, sizeof(zip_archive));

  mz_bool status = mz_zip_reader_init_file(&zip_archive, path.c_str(), 0);

  size_t filenum = (int) mz_zip_reader_get_num_files(&zip_archive);

  kmlfilename.clear();
  kmlcontent.clear();

  for (int i = 0; i < filenum; i++) {
    mz_zip_archive_file_stat file_stat;
    mz_zip_reader_file_stat(&zip_archive, i, &file_stat);

    if (0 == strcmp(getFileExtension(file_stat.m_filename).c_str(), "kml")) {
      char *buff = new char[file_stat.m_uncomp_size];
      memset(buff, 0, file_stat.m_uncomp_size);
      bool succ = mz_zip_reader_extract_to_mem(&zip_archive, i, buff, file_stat.m_uncomp_size, 0);

      if (!succ) continue;
      std::string kmlstr(buff);
      kmlfilename.push_back(file_stat.m_filename);
      kmlcontent.push_back(kmlstr);
    }
  }

  mz_zip_reader_end(&zip_archive);

  return kmlcontent.size();
}

size_t readKML(std::string path, std::vector<std::string> &kmlfilename, std::vector<std::string> &kmlcontent) {
  kmlfilename.clear();
  kmlcontent.clear();

  kmlfilename.push_back(path);
  std::ifstream ifs(path);
  ifs.seekg(0, std::ios::end);
  size_t flsz = ifs.tellg();
  ifs.seekg(0, std::ios::beg);
  char *buff = new char[flsz];
  ifs.read(buff, flsz);
  ifs.close();
  kmlcontent.push_back(std::string(buff));

  return kmlcontent.size();
}

size_t readKMX(std::string path, std::vector<std::string> &kmlfilename, std::vector<std::string> &kmlcontent) {
  if (0 == strcmp(getFileExtension(path).c_str(), "kml")) {
    return readKML(path, kmlfilename, kmlcontent);
  } else if (0 == strcmp(getFileExtension(path).c_str(), "kmz")) {
    return readKMZ(path, kmlfilename, kmlcontent);
  }
  kmlfilename.clear();
  kmlcontent.clear();
  return 0;
}

class PrintNameWalker : public pugi::xml_tree_walker {
 public:
  PrintNameWalker() {}

  virtual ~PrintNameWalker() {}

  // Callback that is called when traversal begins
  virtual bool begin(pugi::xml_node &node) {
    std::cout << "---- PRINT BEGIN ----" << std::endl;
    return true;
  }

  // Callback that is called for each node traversed
  virtual bool for_each(pugi::xml_node &node) {
    for (int i = 0; i < depth(); ++i)
      std::cout << " ";
    std::cout << node.name() << std::endl;
    return true;
  }

  // Callback that is called when traversal ends
  virtual bool end(pugi::xml_node &node) {
    std::cout << "---- PRINT END ----" << std::endl;
    return true;
  }
};

class PrintNameValueWalker : public pugi::xml_tree_walker {
 public:
  PrintNameValueWalker() {}

  virtual ~PrintNameValueWalker() {}

  // Callback that is called when traversal begins
  virtual bool begin(pugi::xml_node &node) {
    std::cout << "---- PRINT BEGIN ----" << std::endl;
    return true;
  }

  // Callback that is called for each node traversed
  virtual bool for_each(pugi::xml_node &node) {
    for (int i = 0; i < depth(); ++i)
      std::cout << " ";
    std::cout << node.name() << ":" << node.value() << std::endl;
    return true;
  }

  // Callback that is called when traversal ends
  virtual bool end(pugi::xml_node &node) {
    std::cout << "---- PRINT END ----" << std::endl;
    return true;
  }
};

class PolygonWalker : public pugi::xml_tree_walker {
 public:
  typedef std::vector<double> Point;
  typedef std::vector<Point> Polygon;

  struct PolygonStruct {
    std::string path;
    Polygon polygon;
  };

  PolygonWalker() {}

  virtual ~PolygonWalker() {}

  std::vector<PolygonStruct> &polygons() { return _polygons; }
  const std::vector<PolygonStruct> &polygons() const { return _polygons; }

  // Callback that is called when traversal begins
  virtual bool begin(pugi::xml_node &node) {
    _polygons.clear();
    return true;
  }

  // Callback that is called for each node traversed
  virtual bool for_each(pugi::xml_node &node) {
    if (std::string(node.name()) == "Polygon") {
      PolygonStruct ps;
      /// An intuitive way, visit nodes as tree.
      pugi::xml_node coordsNode = node.child("outerBoundaryIs").child("LinearRing").child("coordinates");
      ps.path = node.path('/');
      std::string coords = coordsNode.child_value();
      std::vector<std::string> lines = split(coords, ' ');
      for (int i = 0; i < lines.size(); ++i) {
        std::vector<std::string> xy = split(lines[i], ',');
        if (xy.empty()) continue;
        std::vector<double> dbxy;
        dbxy.push_back(std::atof(xy[0].c_str()));
        dbxy.push_back(std::atof(xy[1].c_str()));
        ps.polygon.push_back(dbxy);
      }
      _polygons.push_back(ps);
    }
    return true;
  }

  // Callback that is called when traversal ends
  virtual bool end(pugi::xml_node &node) {
    return true;
  }
 private:
  std::vector<PolygonStruct> _polygons;
};

class PlacemarkWalker : public pugi::xml_tree_walker {
 public:
  typedef std::vector<double> Point;
  typedef std::vector<Point> Polygon;

  struct PlacemarkStruct {
    std::string path;
    std::string id;
    std::string name;
    Polygon polygon;
  };

  PlacemarkWalker() {}

  virtual ~PlacemarkWalker() {}

  std::vector<PlacemarkStruct> &placemarks() { return _placemarks; }
  const std::vector<PlacemarkStruct> &placemarks() const { return _placemarks; }

  // Callback that is called when traversal begins
  virtual bool begin(pugi::xml_node &node) {
    _placemarks.clear();
    return true;
  }

  // Callback that is called for each node traversed
  virtual bool for_each(pugi::xml_node &node) {
    if (std::string(node.name()) == "Placemark") {
      PlacemarkStruct ps;
      ps.id = node.attribute("id").value();
      ps.name = node.child_value("name");
      ps.path = node.path('/');

      /// Another way to visit node with path
      /// Select_nodes can perform query with query sentence https://www.w3.org/TR/1999/REC-xpath-19991116/
      pugi::xpath_node coordsNode = node.select_node("MultiGeometry/Polygon/outerBoundaryIs/LinearRing/coordinates");

      std::string coords = coordsNode.node().child_value();
      std::vector<std::string> lines = split(coords, ' ');
      for (int i = 0; i < lines.size(); ++i) {
        std::vector<std::string> xy = split(lines[i], ',');
        if (xy.empty()) continue;
        std::vector<double> dbxy;
        dbxy.push_back(std::atof(xy[0].c_str()));
        dbxy.push_back(std::atof(xy[1].c_str()));
        ps.polygon.push_back(dbxy);
      }
      _placemarks.push_back(ps);
    }
    return true;
  }

  // Callback that is called when traversal ends
  virtual bool end(pugi::xml_node &node) {
    return true;
  }
 private:
  std::vector<PlacemarkStruct> _placemarks;
};

#endif //TINYKML_TINYKML_H
