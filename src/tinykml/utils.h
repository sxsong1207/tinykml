// Copyright 2018 Shaun Song <sxsong1207@qq.com>
//
// Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
// rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
// permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all copies or substantial portions of the
// Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
// COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
#ifndef TINYKML_UTILS_H
#define TINYKML_UTILS_H

#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <stdlib.h>
#include <regex>

#include <miniz/miniz.h>
#include <miniz/miniz_zip.h>

std::string getFileExtension(const std::string &path) {
  std::string::size_type sep_idx0 = path.rfind('/');
  std::string::size_type sep_idx1 = path.rfind('\\');
  if (sep_idx0 == std::string::npos) sep_idx0 = 0;
  if (sep_idx1 == std::string::npos) sep_idx1 = 0;
  std::string::size_type sep_idx = std::max(sep_idx0, sep_idx1);
  std::string::size_type idx = path.rfind('.');
  if (idx == std::string::npos || idx < sep_idx) return "";
  return path.substr(idx+1);
}


std::vector<std::string> split(const std::string &s, char delimiter) {
  std::vector<std::string> tokens;
  std::string token;
  std::istringstream tokenStream(s);
  while (std::getline(tokenStream, token, delimiter)) {
    tokens.push_back(token);
  }
  return tokens;
}

std::vector<std::string> resplit(const std::string &s, std::string rgx_str = "\\s+") {

  std::vector<std::string> elems;

  std::regex rgx(rgx_str);

  std::sregex_token_iterator iter(s.begin(), s.end(), rgx, -1);
  std::sregex_token_iterator end;

  while (iter != end) {
    elems.push_back(*iter);
    ++iter;
  }
  return elems;
}

#endif //TINYKML_UTILS_H
