TinyKML
======

pugixml is a lightweight C++ KML lib for OGC KML parsing with modern C++ xml parser [pugixml](https://github.com/zeux/pugixml).

The library is super simple than [libkml](https://github.com/google/libkml). 

